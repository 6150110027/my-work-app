package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        WebView Web1 = findViewById(R.id.W1);
        Web1.setWebViewClient(new WebViewClient());
        Web1.loadUrl("https://www.facebook.com/phanthana.mongkholjamorn.16");
    }
}